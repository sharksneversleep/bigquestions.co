module.exports = function(grunt) {

    var pngquant = require('imagemin-pngquant');
    var mozjpeg  = require('imagemin-mozjpeg');
    var gifsicle = require('imagemin-gifsicle');


    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            // 2. Configuration for concatinating files goes here.
            dist: {
                src: [
                    '../js/**/*.js',
                    '!../js/modernizr.js',
                    '!../js/build/**/*.js',
                    //'!../js/library/**/*.js',
                    '!../js/vendor/**/*.js'
                ],
                dest: '../js/build/production.js',
            }
        },

        uglify: {
            build: {
                src: '../js/build/production.js',
                dest: '../js/build/production.min.js'
            }
        },

        imagemin: {
            dynamic: {
                options: {
                    optimizationLevel: 5,
                    progressive: true,
                    svgoPlugins: [{ removeViewBox: false }],
                    use: [pngquant(), mozjpeg(), gifsicle()]
                },
                files: [{
                    expand: true,
                    cwd: '../img/',
                    src: ['**/*.{png,jpg,jpeg,gif}'],
                    dest: '../img/'
                }]
            }
        },

        svgmin: { //minimize SVG files
            options: {
                plugins: [
                    { removeViewBox: false },
                    { removeUselessStrokeAndFill: false }
                ]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '../img/',
                    src: ['**/*.svg'],
                    dest: '../img/'
                }]
            }
        }

    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-svgmin');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['concat', 'uglify']);
    grunt.registerTask('image', ['imagemin', 'svgmin']);

};